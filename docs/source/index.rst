.. xarray_dic documentation master file, created by
   sphinx-quickstart on Mon Aug  2 15:27:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to xarray_dic's documentation!
======================================

``xarray_dic`` is a `xarray <http://xarray.pydata.org/en/stable/>`_ accessors to work on output from Digital Images Correlation

In this documentation detail of the functions are given. For quick start script to use this accessors we recommend the `DIC Book <https://mecaiceige.gricad-pages.univ-grenoble-alpes.fr/tools/documentations/DIC-book/docs/intro.html>`_

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Installation
============

From repository
***************

.. code:: bash
    
    git clone https://gricad-gitlab.univ-grenoble-alpes.fr/mecaiceige/tools/lib_python/xarray_dic
    cd xarray_dic
    python -m pip install -r requirements.txt
    pip install -e .

Functions Overview
==================

.. toctree::
    :maxdepth: 0
    :numbered: 
    
    func_o

Contact
=======
:Author: Thomas Chauve
:Contact: thomas.chauve@univ-grenoble-alpes.fr

:Organization: `IGE <https://www.ige-grenoble.fr/>`_
:Status: This is a "work in progress"
:Version: 0.1

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
