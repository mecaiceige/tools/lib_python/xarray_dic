Functions overview
==================

Loading data
************
.. automodule:: xarray_dic.loadDIC
	:special-members:
	:members:

xarray_dic xr.Dataset accessor
******************************
.. automodule:: xarray_dic.xarray_dic
	:special-members:
	:members:
