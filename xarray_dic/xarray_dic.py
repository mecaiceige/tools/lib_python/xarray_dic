import xarray as xr
import numpy as np

@xr.register_dataset_accessor("dic")

class xarray_dic(object):
    '''
    This is a classe to work on aita data in xarray environnement.
    
    .. note:: xarray does not support heritage from xr.DataArray may be the day it support it, we could move to it
    '''
    
    def __init__(self, xarray_obj):
        '''
        Constructor for aita. 
        
        The xarray_obj should contained at least :
        1. displacement : DataArray that contained displacement in the last column of dimention
        2. strain : DataArray taht contained strain field compatible with xarray_symTensor2d
        
        :param xarray_obj:
        :type xarray_obj: xr.Dataset
        '''
        self._obj = xarray_obj 
    pass
#----------------------------------------------------------------------------------------------------------
    def DIC_line(self,axis='y',shift=3):
        '''
        Compute the average of one componant
        
        :param axis: txx,tyy,tzz,txy,txz,tyz
        :type axis: str
        :return mean: average of this component
        :rtype mean: xr.DataArray
        '''
        
        if axis=='x':
            sd=(np.nanmean(self._obj.displacement[...,:,-shift,0],axis=-1)-np.nanmean(self._obj.displacement[...,:,shift,0],axis=-1))/np.array((self._obj.x[-shift]-self._obj.x[shift]))
        if axis=='y':
            sd=(np.nanmean(self._obj.displacement[...,-shift,:,1],axis=-1)-np.nanmean(self._obj.displacement[...,shift,:,1],axis=-1))/np.array((self._obj.y[-shift]-self._obj.y[shift]))
            
        return xr.DataArray(sd,dims=self._obj.displacement.coords.dims[0])

#-----------------------------------------------------------------------------------------------------------
    def find_pic(self,strain_step=-0.005,a_im=1,b_im=0,var_macro=None,**kwargs):
        '''
        Find picture number as between 2 picture the increment of deformation is macro_strain
        
        :param strain_step: macroscopic strain between 2 picture.
        :type strain_step: float
        :param a_im: value to give true output file number image a_im*n+b_im
        :type a_im: int
        :param b_im: value to give true output file number image a_im*n+b_im
        :type b_im: im
        :param var_macro: name of the variable to use for macro strain. If None compute the macro from dic_line.
        :type var_macro: string
        :return: nb_img
        :rtype: array
        '''
        
        ds=self._obj
        # extract macroscopic strain from the data
        if var_macro is None:
            var_macro='dl'
            ds[var_macro]=self.DIC_line(**kwargs)
        
        nb_img=[]
        nb_img.append(0)
        macro_line=np.array(ds[var_macro])
        
        id=np.isnan(macro_line)
        macro_line[id]=0

        for i in range(int((macro_line[-1]-macro_line[0])/strain_step)):
            # find where the macro strain is higher that the step
            idx=np.where(macro_line>np.float(i+1)*strain_step+macro_line[0])
            # take the minimum
            label=np.where(macro_line==np.min(macro_line[idx]))
            nb_img.append(int(label[0]))
            
            
        res=a_im*np.array(nb_img)+b_im 
        #np.savetxt('wanted_image.txt',res,fmt='%i')
            
        return res,nb_img
#----------------------------------------------------------------------------------------------------------
    def dic_average(self):
        '''
        Average on the number of correlation done for the same time_step
        '''
        
        if 'ncor' in list(self._obj.dims):
            ds=self._obj.copy()
            ds['displacement']=self._obj.displacement.mean(dim='ncor')
            ds['std_displacement']=self._obj.displacement.std(dim='ncor')
            ds['strain']=self._obj.strain.mean(dim='ncor')
            ds['std_strain']=self._obj.strain.std(dim='ncor')
            
            return ds
            
            
        else:
            return print('ERROR : only on time step is avilaible. Use multi_load function to load more correaltion for the same time step')
