import xarray as xr
import numpy as np
import skimage.morphology
import os
import natsort
import vtk
from vtk.util.numpy_support import vtk_to_numpy
from tqdm import trange

def loadSpam(adr_data,resolution, time_step, unit_time='seconds',unit_res='millimeter',strain_com=1):
    '''
    Load output from spam after spam.ldic
    
    :param adr_data: folder path where the 7D output are store
    :type adr_data: str
    :param resolution: pixel size of the image used for DIC (millimeters)
    :type resolution: float
    :param time_step: time step between the picture (seconds)
    :type time_step: float
    :param adr_micro: path for the black and white skeleton of the microstructure (bmp format) (default 0 - no microstructure)
    :type adr_micro: str
    '''
    # include time_step in the object
    time=time_step
    # find out file from 7D
    output=os.listdir(adr_data)
    output.sort()
    
    all_da=[]
    all_sT=[]

    for i in range(len(output)):
        reader = vtk.vtkDataSetReader() 
        reader.SetFileName(adr_data+output[i])
        reader.Update()
        ug  = reader.GetOutput()
        n_dic=ug.GetSpacing()[0]
        ugdim=ug.GetDimensions()
        v=np.flipud(vtk_to_numpy(ug.GetCellData().GetArray(0)).reshape([ugdim[1]-1,ugdim[0]-1,3]))*resolution
        da=xr.DataArray(v[:,:,0:2],dims=['y','x','vc'])


        dx=np.linspace(0.5,ugdim[0]-1.5,ugdim[0]-1)*n_dic*resolution
        dy=np.linspace(0.5,ugdim[1]-1.5,ugdim[1]-1)*n_dic*resolution

        da.coords['x']=dx
        da.coords['y']=dy
                
        if i==0:
            mask=da.sum(axis=-1)!=0.    
        
        #print(da.x)
        #da=da.where(mask,drop=True)
        #da=da.where(da.sum(axis=-1)!=0)
        da[:,:,1]=-da[:,:,1]
        
               
        #print(da.x)
        all_da.append(np.array(da))
        
        
        diff_ux=da[:,:,0].differentiate('x')
        diff_vx=da[:,:,1].differentiate('x')

        diff_uy=da[:,:,0].differentiate('y')
        diff_vy=da[:,:,1].differentiate('y')

        empty=np.zeros(diff_ux.shape[0:2])
        empty[:,:]=np.nan
        # exx, eyy, ezz, exy, exz, eyz
        if strain_com==0:
            sT=np.dstack((
                np.array(diff_ux),
                np.array(diff_vy),
                empty,
                np.array((diff_uy+diff_vx)*0.5),
                empty,
                empty,
            ))
        else:
            sT=np.dstack((
                np.array(diff_ux+(diff_ux**2+diff_vx**2)*0.5),
                np.array(diff_vy+(diff_uy**2+diff_vy**2)*0.5),
                empty,
                np.array((diff_uy+diff_vx)*0.5+(diff_ux*diff_uy+diff_vx*diff_vy)*0.5),
                empty,
                empty,
            ))

        idx,idy=np.where((np.isnan(sT[:,:,0])+np.isnan(sT[:,:,1])+np.isnan(sT[:,:,3]))==True)
        if len(idx)!=0:
            sT[idx,idy,:]=np.nan

        all_sT.append(sT)
        
    dsT=xr.DataArray(np.stack(all_sT),dims=['time','y','x','sT'])
    da=xr.DataArray(np.stack(all_da),dims=['time','y','x','d'])

    #ss=da.shape
    #dx=np.linspace(0,ss[2]-1,ss[2])*n_dic*resolution
    #dy=np.linspace(0,ss[1]-1,ss[1])*n_dic*resolution

    time=np.linspace(time_step,time_step*len(output),len(output))

    ds = xr.Dataset(
    {   
        "displacement": da,
        "strain": dsT,

    },
    )
    #ds.coords['x']=da.x
    #ds.coords['y']=da.y
    ds.coords['time']=time

    ds.attrs["unit_time"]=unit_time
    ds.attrs["step_size"]=resolution
    ds.attrs["unit_position"]=unit_res
    ds.attrs["window_size"]=int(n_dic)
    ds.attrs["path_dat"]=adr_data
    ds.attrs["DIC_software"]='spam'
    
    mm=skimage.morphology.erosion(mask)
    mask[:,:]=mm
    ds=ds.where(mask,drop=True)
    
    return ds

def loadSpam_strain(adr_data,resolution, time_step, unit_time='seconds',unit_res='millimeter'):
    '''
    Load output from spam after spam-regularStrain
    
    :param adr_data: folder path where the 7D output are store
    :type adr_data: str
    :param resolution: pixel size of the image used for DIC (millimeters)
    :type resolution: float
    :param time_step: time step between the picture (seconds)
    :type time_step: float
    :param adr_micro: path for the black and white skeleton of the microstructure (bmp format) (default 0 - no microstructure)
    :type adr_micro: str
    '''
    # include time_step in the object
    time=time_step
    # find out file from 7D
    output=os.listdir(adr_data)
    output.sort()
    
    all_da=[]
    all_sT=[]

    for i in range(len(output)):
        reader = vtk.vtkDataSetReader() 
        reader.SetFileName(adr_data+output[i])
        reader.Update()
        ug  = reader.GetOutput()
        n_dic=ug.GetSpacing()[0]
        ugdim=ug.GetDimensions()
        e=np.flipud(vtk_to_numpy(ug.GetCellData().GetArray('e')).reshape([ugdim[1]-1,ugdim[0]-1,9]))
        
        empty=np.zeros(e[:,:,0].shape[0:2])
        empty[:,:]=np.nan
        
        sT=np.dstack((
            e[:,:,0],
            e[:,:,4],
            empty,
            e[:,:,1],
            empty,
            empty,
        ))
        
        de=xr.DataArray(e,dims=['y','x','sT'])
        
        
        dx=np.linspace(0,ugdim[0]-2,ugdim[0]-1)*n_dic*resolution
        dy=np.linspace(0,ugdim[1]-2,ugdim[1]-1)*n_dic*resolution

        de.coords['x']=dx
        de.coords['y']=dy
        
        da=xr.DataArray(np.dstack((empty,empty)),dims=['y','x','vc'])
        da.coords['x']=dx
        da.coords['y']=dy
        
        all_da.append(np.array(da))


        
        # exx, eyy, ezz, exy, exz, eyz
        sT=np.dstack((
            e[:,:,0],
            e[:,:,4],
            empty,
            e[:,:,1],
            empty,
            empty,
        ))

        all_sT.append(sT)
        
    dsT=xr.DataArray(np.stack(all_sT),dims=['time','y','x','sT'])
    da=xr.DataArray(np.stack(all_da),dims=['time','y','x','d'])

    time=np.linspace(time_step,time_step*len(output),len(output))

    ds = xr.Dataset(
    {   
        "displacement": da,
        "strain": dsT,

    },
    )
    ds.coords['x']=dx
    ds.coords['y']=dy
    ds.coords['time']=time

    ds.attrs["unit_time"]=unit_time
    ds.attrs["step_size"]=resolution
    ds.attrs["unit_position"]=unit_res
    ds.attrs["window_size"]=int(n_dic)
    ds.attrs["path_dat"]=adr_data
    ds.attrs["DIC_software"]='spam-regularStrain'
    
    ds=ds.where(ds.strain[0,:,:,0]!=0,drop=True)
        
    return ds
        
        
    

def load7D(adr_data, resolution, time_step,unit_time='seconds',unit_res='millimeter'):
    '''
    Load output from 7D from gdr export option
    
    :param adr_data: folder path where the 7D output are store
    :type adr_data: str
    :param resolution: pixel size of the image used for DIC (millimeters)
    :type resolution: float
    :param time_step: time step between the picture (seconds)
    :type time_step: float
    :param adr_micro: path for the black and white skeleton of the microstructure (bmp format) (default 0 - no microstructure)
    :type adr_micro: str
    '''

    # include time_step in the object
    time=time_step

    # find out file from 7D
    output=os.listdir(adr_data)
    output.sort()
    # loop on all the output
    all_sT=[]
    all_da=[]
    for i in list(range(len(output))):
        # load the file
        data=np.loadtxt(adr_data + '/' + output[i])
        # find not indexed point
        id=np.where(data[:,4]==0.0)
        # replace not indexed point by NaN value
        data[id,2:4]=np.nan
        data[id,5:8]=np.nan
        dx=np.unique(data[:,0])*resolution
        dy=np.unique(data[:,1])*resolution
        # for the first step it extract size sx,sy and window correlation value used in 7D
        if (i==0):
            n_dic=np.abs(data[0,1]-data[1,1])
            nb_pix=np.size(data[:,0])
            sx=np.int32(np.abs(data[0,0]-data[nb_pix-1,0])/n_dic+1)
            sy=np.int32(np.abs(data[0,1]-data[nb_pix-1,1])/n_dic+1)
            eij=np.zeros([sy,sx])
            eij[:,:]=np.nan

        # Build image at time t=i
        u=np.flipud(np.transpose(np.reshape(data[:,2]*resolution,[sx,sy])))
        v=np.flipud(np.transpose(np.reshape(data[:,3]*resolution,[sx,sy])))
        exx=np.flipud(np.transpose(np.reshape(data[:,5],[sx,sy])))
        eyy=np.flipud(np.transpose(np.reshape(data[:,6],[sx,sy])))
        exy=np.flipud(np.transpose(np.reshape(data[:,7],[sx,sy])))
        all_sT.append(np.dstack((exx,eyy,eij,exy,eij,eij)))
        all_da.append(np.dstack((u,v)))
        #dx=np.linspace(0,n_dic*resolution*(sx-1),sx)
        #dy=np.linspace(0,n_dic*resolution*(sy-1),sy)

    dsT=xr.DataArray(np.stack(all_sT),dims=['time','y','x','sT'])
    da=xr.DataArray(np.stack(all_da),dims=['time','y','x','d'])

    time=np.linspace(time_step,time_step*len(output),len(output))

    ds = xr.Dataset(
    {   
        "displacement": da,
        "strain": dsT,

    },
    )
    ds.coords['x']=dx
    ds.coords['y']=dy
    ds.coords['time']=time

    ds.attrs["unit_time"]=unit_time
    ds.attrs["step_size"]=resolution
    ds.attrs["unit_position"]=unit_res
    ds.attrs["window_size"]=int(n_dic)
    ds.attrs["path_dat"]=adr_data
    ds.attrs["DIC_software"]='7D'

    return ds
    

def loadDICe(adr_data, resolution, time_step,unit_time='seconds',unit_res='millimeter',strain_com=1):
    '''
    Load DIC data from DICe software

    :param adr_data: folder path where the DICe output are store
    :type adr_data: str
    :param resolution: pixel size of the image used for DIC (millimeters)
    :type resolution: float
    :param time_step: time step between the picture (seconds)
    :type time_step: float
    :param adr_micro: path for the black and white skeleton of the microstructure (bmp format) (default 0 - no microstructure)
    :type adr_micro: str
    :param strain_com: Computation method for strain calculation 0-small deformation 1-Green Lagrange (default 1)
    :type strain_com: bool
    '''

    # include time_step in the object
    time=time_step

    # find out file from 7D
    output=os.listdir(adr_data+'results/')
    output.sort()
    output=output[2:-1]
    # loop on all the output
    all_sT=[]
    all_da=[]
    for i in list(range(len(output))):
    #for i in [0]:
        # load the file
        data=np.loadtxt(adr_data+'results/'+ output[i],skiprows=1,delimiter=',',usecols=[1,2,3,4,9])
        # find not indexed point
        id=np.where(data[:,-1]==0)
        # replace not indexed point by NaN value
        data[id[0],2]=np.nan
        data[id[0],3]=np.nan
        # for the first step it extract size sx,sy and window correlation value used in 7D
        if (i==0):
            n_dic=np.abs(data[0,1]-data[1,1])
            nb_pix=np.size(data[:,0])
            sx=np.int32(1+np.abs(np.min(data[:,0])-np.max(data[:,0]))/n_dic)
            sy=np.int32(1+np.abs(np.min(data[:,1])-np.max(data[:,1]))/n_dic)
            dx=np.linspace(0,n_dic*resolution*(sx-1),sx)
            dy=np.linspace(0,n_dic*resolution*(sy-1),sy)
        
        # Build image at time t=i
        tmp_u=np.zeros([sx,sy])
        tmp_u[:,:]=np.nan
        tmp_v=np.zeros([sx,sy])
        tmp_v[:,:]=np.nan
        # find x y position in the table
        x=(data[:,0]-np.min(data[:,0]))/n_dic
        y=(data[:,1]-np.min(data[:,1]))/n_dic
        # include data
        tmp_u[x.astype(int),y.astype(int)]=data[:,2]
        tmp_v[x.astype(int),y.astype(int)]=data[:,3]
        # Create im2d data object u and v
        imu=np.flipud(np.transpose(tmp_u*resolution))
        imv=-np.flipud(np.transpose(tmp_v*resolution))
                
        da=xr.DataArray(np.dstack((imu,imv)),dims=['y','x','vc'])
        da.coords['x']=dx
        da.coords['y']=dy
        all_da.append(np.dstack((imu,imv)))
        
        diff_ux=da[:,:,0].diff('x')/(resolution*n_dic)
        diff_ux=diff_ux.where(diff_ux.y>0,drop=True)
        diff_vx=da[:,:,1].diff('x')/(resolution*n_dic)
        diff_vx=diff_vx.where(diff_vx.y>0,drop=True)
        
        diff_uy=da[:,:,0].diff('y')/(resolution*n_dic)
        diff_uy=diff_uy.where(diff_uy.x>0,drop=True)
        diff_vy=da[:,:,1].diff('y')/(resolution*n_dic)
        diff_vy=diff_vy.where(diff_vy.x>0,drop=True)
        
        empty=np.zeros(diff_ux.shape[0:2])
        empty[:,:]=np.nan
        # exx, eyy, ezz, exy, exz, eyz
        if strain_com==0:
            sT=np.dstack((
                np.array(diff_ux),
                np.array(diff_vy),
                empty,
                np.array((diff_uy+diff_vx)*0.5),
                empty,
                empty,
            ))
        else:
            sT=np.dstack((
                np.array(diff_ux+(diff_ux**2+diff_vx**2)*0.5),
                np.array(diff_vy+(diff_uy**2+diff_vy**2)*0.5),
                empty,
                np.array((diff_uy+diff_vx)*0.5+(diff_ux*diff_uy+diff_vx*diff_vy)*0.5),
                empty,
                empty,
            ))
        
        
        idx,idy=np.where((np.isnan(sT[:,:,0])+np.isnan(sT[:,:,1])+np.isnan(sT[:,:,3]))==True)
        if len(idx)!=0:
            sT[idx,idy,:]=np.nan
        
        all_sT.append(sT)
       
    dsT=xr.DataArray(np.stack(all_sT),dims=['time','yt','xt','sT'])
    da=xr.DataArray(np.stack(all_da),dims=['time','y','x','d'])
    
    time=np.linspace(time_step,time_step*len(output),len(output))
    
    ds = xr.Dataset(
    {   
        "displacement": da,
        "strain": dsT,

    },
    )
    ds.coords['x']=dx
    ds.coords['y']=dy
    ds.coords['xt']=dx[1::]
    ds.coords['yt']=dy[1::]
    ds.coords['time']=time

    ds.attrs["unit_time"]=unit_time
    ds.attrs["step_size"]=resolution
    ds.attrs["unit_position"]=unit_res
    ds.attrs["window_size"]=int(n_dic)
    ds.attrs["path_dat"]=adr_data
    ds.attrs["DIC_software"]='DICe'
            
            
    return ds

def loadLMGC(adr_data,resolution, time_step, unit_time='seconds',unit_res='millimeter',strain_com=1):
    '''
    Load output from spam after spam.ldic
    
    :param adr_data: folder path where the 7D output are store
    :type adr_data: str
    :param resolution: pixel size of the image used for DIC (millimeters)
    :type resolution: float
    :param time_step: time step between the picture (seconds)
    :type time_step: float
    :param adr_micro: path for the black and white skeleton of the microstructure (bmp format) (default 0 - no microstructure)
    :type adr_micro: str
    '''
    # include time_step in the object
    time=time_step
    # find out file from 7D
    output=[]

    for file in os.listdir(adr_data): 
        if file.endswith(".dep"):
            output.append(file)

    output=natsort.natsorted(output)
    
    all_da=[]
    all_sT=[]

    for i in trange(len(output)):
        # displacement
        data=np.loadtxt(adr_data + '/' + output[i],skiprows=26)
        # shape
        f = open(adr_data + '/' + output[0])
        ss = f.readlines()
        ss_x=int(ss[25].split()[-1])
        ss_y=int(ss[24].split()[-1])
        
        n_dic=int(ss[22].split()[-1])

        if data.shape[0]!=ss_x*ss_y:
            new_data=np.zeros([ss_x*ss_y,2])
            new_data[:,:]=np.nan
            id=np.int32(data[:,4])
            new_data[id,0]=data[:,2]
            new_data[id,1]=data[:,3]

            vv=new_data
        else:
            vv=data[:,2:4]
        

        v=np.flipud(vv.flatten().reshape([ss_y,ss_x,2]))*resolution
        da=xr.DataArray(v,dims=['y','x','vc'])


        dx=np.linspace(0.5,ss_x-1.5,ss_x)*n_dic*resolution
        dy=np.linspace(0.5,ss_y-1.5,ss_y)*n_dic*resolution

        da.coords['x']=dx
        da.coords['y']=dy
                
        if i==0:
            mask=da.sum(axis=-1)!=0.    
        
        #print(da.x)
        #da=da.where(mask,drop=True)
        #da=da.where(da.sum(axis=-1)!=0)
        da[:,:,1]=da[:,:,1]
        
               
        #print(da.x)
        all_da.append(np.array(da))
        
        
        diff_ux=da[:,:,0].differentiate('x')
        diff_vx=da[:,:,1].differentiate('x')

        diff_uy=da[:,:,0].differentiate('y')
        diff_vy=da[:,:,1].differentiate('y')

        empty=np.zeros(diff_ux.shape[0:2])
        empty[:,:]=np.nan
        # exx, eyy, ezz, exy, exz, eyz
        if strain_com==0:
            sT=np.dstack((
                np.array(diff_ux),
                np.array(diff_vy),
                empty,
                np.array((diff_uy+diff_vx)*0.5),
                empty,
                empty,
            ))
        else:
            sT=np.dstack((
                np.array(diff_ux+(diff_ux**2+diff_vx**2)*0.5),
                np.array(diff_vy+(diff_uy**2+diff_vy**2)*0.5),
                empty,
                np.array((diff_uy+diff_vx)*0.5+(diff_ux*diff_uy+diff_vx*diff_vy)*0.5),
                empty,
                empty,
            ))

        idx,idy=np.where((np.isnan(sT[:,:,0])+np.isnan(sT[:,:,1])+np.isnan(sT[:,:,3]))==True)
        if len(idx)!=0:
            sT[idx,idy,:]=np.nan

        all_sT.append(sT)
        
    dsT=xr.DataArray(np.stack(all_sT),dims=['time','y','x','sT'])
    da=xr.DataArray(np.stack(all_da),dims=['time','y','x','d'])

    #ss=da.shape
    #dx=np.linspace(0,ss[2]-1,ss[2])*n_dic*resolution
    #dy=np.linspace(0,ss[1]-1,ss[1])*n_dic*resolution

    time=np.linspace(time_step,time_step*len(output),len(output))

    ds = xr.Dataset(
    {   
        "displacement": da,
        "strain": dsT,

    },
    )
    #ds.coords['x']=da.x
    #ds.coords['y']=da.y
    ds.coords['time']=time

    ds.attrs["unit_time"]=unit_time
    ds.attrs["step_size"]=resolution
    ds.attrs["unit_position"]=unit_res
    ds.attrs["window_size"]=int(n_dic)
    ds.attrs["path_dat"]=adr_data
    ds.attrs["DIC_software"]='LMGC'
    
    mm=skimage.morphology.erosion(mask)
    mask[:,:]=mm
    ds=ds.where(mask,drop=True)
    
    return ds

def multi_load(func_load, adr, res, time_step, **kwargs):
    '''
    Open multiple correlation between t1 qnd t2
    
    :param func_load: the function that you want to use for loading the data
    :param adr: path to the folder where each folder contained a correaltion set
    :type adr: str
    
    .. note:: **kwarg you should also pass all the argument needed for you loading function
    '''
    subcor=natsort.natsorted(os.listdir(adr))
    
    alldata=[]
    tt=1
    for sadr in subcor:
        tds=func_load(adr+sadr+'/',res,time_step,**kwargs)
        if tt:
            tt=0
            d1=tds.displacement.expand_dims(dim='ncor')
            sT1=tds.strain.expand_dims(dim='ncor')
        else:
            td1=tds.displacement.expand_dims(dim='ncor')
            tsT1=tds.strain.expand_dims(dim='ncor')
            d1=xr.concat([d1,td1], dim="ncor")
            sT1=xr.concat([sT1,tsT1], dim="ncor")
            
    ds=xr.Dataset()
    ds['displacement']=d1
    ds['strain']=sT1
    ds.coords['x']=tds.x
    ds.coords['y']=tds.y
    ds.coords['time']=tds.time
    ds.attrs['unit_time']=tds.unit_time
    ds.attrs['step_size']=tds.step_size
    ds.attrs['unit_position']=tds.unit_position
    ds.attrs['window_size']=tds.window_size
    ds.attrs['path_dat']=adr
    ds.attrs['DIC_software']=tds.DIC_software
    
    return ds
    
    
